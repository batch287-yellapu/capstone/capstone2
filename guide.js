Session 37:
	touch index.js .gitignore guide.js
	npm init --y
	npm install express mongoose cors
	create our Product Model
	create our User Model

Session 38:
	create a route for register user
	create a route for login user
	npm install bcrypt
		use bcrypt in user s password
	npm install jsonwebtoken

Session 39:
	refactor the user details, added authorization
	create a route for create product, added authorization and only admin can create product

Session 40:
	create a route for retrieving all active products
	create a route for retrieving a specific product
	create a route for updating a product
	create a route for archiving a product

Session 41:
	create a route for ordering products
	
