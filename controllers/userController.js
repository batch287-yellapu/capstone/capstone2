const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (reqBody) => {

	console.log(reqBody);

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {

	// .findOne() will look for the first document that matches
	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		// Email doesn't exist
		if(result == null){

			return false;

		} else {

			// We now know that the email is correct, is password correct also?
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			console.log(isPasswordCorrect);
			console.log(result);

			// Correct password
			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result) }

			// Password incorrect	
			} else {

				return false
				
			};

		};
	});
};

module.exports.getProfile = (reqParams) => {

	return User.findById(reqParams.userId).then(result => {
		result.password = "";
		// Returns the user information with the password as an empty string
		return result;
	})
}


module.exports.checkout = async (data) => {

	// console.log(data);

	if(!data.isAdmin){
		let amount = 0;
		for (let i = 0; i < data.products.length; i++) {
 			 console.log(data.products[i].productId);
 			 let calculation = await Product.findById(data.products[i].productId).then(product => 
			{amount += product.price*data.products[i].quantity;
			});
		}


		let newOrder = new Order({
			userId: data.userId,
			products: data.products,
			totalAmount: amount
		});

		console.log(newOrder)
		return newOrder.save().then((order, error) => {

			if(error){

				return false;

			} else {

				return true;
			}
		})

	}
	
// Since Promise.resolve() returns a resolved promise, the variable message will already be in a resolved status.
let message = Promise.resolve("User must be an NON-Admin to access this.");
return message.then((value) => {
	return value;
});
};
