const Product = require("../models/Product");

module.exports.createProduct = (data) => {

	console.log(data);

	if(data.isAdmin){

		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		return newProduct.save().then((product, error) => {

			if(error){

				return false;

			} else {

				return true;
			}
		})

	}
	
// Since Promise.resolve() returns a resolved promise, the variable message will already be in a resolved status.
let message = Promise.resolve("User must be an Admin to access this.");
return message.then((value) => {
	return value;
});
};

module.exports.activeProducts = () => {

	return Product.find({isActive : true}).then(result => {
		return result;
	});
};

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		Name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};

module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});

};

