const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication (login)
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving user details
router.get("/:userId/userDetails", auth.verify, (req, res) => {

	console.log(req.params.productId);

	userController.getProfile(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for ordering products
router.post("/checkout", auth.verify, (req,res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		products: req.body.products
	};

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;